USE [master]
GO
/****** Object:  Database [CountryInfoDB]    Script Date: 6/15/2015 1:54:55 PM ******/
CREATE DATABASE [CountryInfoDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CountryInfoDB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\CountryInfoDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CountryInfoDB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\CountryInfoDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CountryInfoDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CountryInfoDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CountryInfoDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CountryInfoDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CountryInfoDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CountryInfoDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CountryInfoDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CountryInfoDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CountryInfoDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CountryInfoDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CountryInfoDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CountryInfoDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CountryInfoDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CountryInfoDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CountryInfoDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CountryInfoDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CountryInfoDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CountryInfoDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CountryInfoDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CountryInfoDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CountryInfoDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CountryInfoDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CountryInfoDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CountryInfoDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CountryInfoDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CountryInfoDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CountryInfoDB] SET  MULTI_USER 
GO
ALTER DATABASE [CountryInfoDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CountryInfoDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CountryInfoDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CountryInfoDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CountryInfoDB]
GO
/****** Object:  Table [dbo].[CityEntryTable]    Script Date: 6/15/2015 1:54:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CityEntryTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[About] [varchar](max) NOT NULL,
	[NoOfDwellers] [int] NULL,
	[Location] [varchar](50) NULL,
	[Weather] [varchar](200) NULL,
	[CountryID] [int] NOT NULL,
 CONSTRAINT [PK_CityEntryTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CountryTable]    Script Date: 6/15/2015 1:54:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CountryTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
	[About] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CuontryTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CityEntryTable] ON 

INSERT [dbo].[CityEntryTable] ([ID], [Name], [About], [NoOfDwellers], [Location], [Weather], [CountryID]) VALUES (5, N'Singapore City', N'it is rainy', 120000, N'Town', N'Rainy', 7)
INSERT [dbo].[CityEntryTable] ([ID], [Name], [About], [NoOfDwellers], [Location], [Weather], [CountryID]) VALUES (6, N'Borishal', N'Popular city for youth people', 5000000, N'Beside Khulna', N'Rough ', 1)
INSERT [dbo].[CityEntryTable] ([ID], [Name], [About], [NoOfDwellers], [Location], [Weather], [CountryID]) VALUES (7, N'Delhi', N'Capital', 123123123, N'Kalk', N'Hot', 3)
SET IDENTITY_INSERT [dbo].[CityEntryTable] OFF
SET IDENTITY_INSERT [dbo].[CountryTable] ON 

INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (1, N'Bangladesh', N'Bangladesh is a beautiful country .. and a land of river')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (2, N'India', N'India is developed in economy.')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (3, N'England', N'England is a part of UK')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (4, N'Malaysia', N'<p>Malaysia is best for spending vacation.&nbsp;</p>')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (5, N'USA', N'<p><span style="color: rgb(184, 49, 47); font-size: 17px;"><strong>USA&nbsp;<span style="color: rgb(0, 0, 0);">stand for <span style="color: rgb(97, 189, 109);"><u>United States of America</u></span></span></strong></span></p>')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (6, N'Singapore', N'<p class="f-typewriter"><span style="color: rgb(184, 49, 47); font-family: Courier,Courier New; font-size: 20px;"><u><em>Singapore</em></u></span> <u>is city of Travel. People are co operate</u></p>')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (1006, N'Nigeria', N'<p>Nigeria <br><span style="color: rgb(251, 160, 38);">Region : Africa</span><br></p><p><br></p>')
INSERT [dbo].[CountryTable] ([ID], [CountryName], [About]) VALUES (1007, N'Russia', N'<p>Russia is one of the most powerful country</p>')
SET IDENTITY_INSERT [dbo].[CountryTable] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CityEntryTable]    Script Date: 6/15/2015 1:54:55 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CityEntryTable] ON [dbo].[CityEntryTable]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CountryTable]    Script Date: 6/15/2015 1:54:55 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CountryTable] ON [dbo].[CountryTable]
(
	[CountryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CountryTable]  WITH CHECK ADD  CONSTRAINT [FK_CountryTable_CountryTable] FOREIGN KEY([ID])
REFERENCES [dbo].[CountryTable] ([ID])
GO
ALTER TABLE [dbo].[CountryTable] CHECK CONSTRAINT [FK_CountryTable_CountryTable]
GO
USE [master]
GO
ALTER DATABASE [CountryInfoDB] SET  READ_WRITE 
GO
