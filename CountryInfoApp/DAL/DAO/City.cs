﻿namespace CountryInfoApp.DAL.DAO
{
    public class City
    {
        public string Name { get; set; }
        public string  About { get; set; }

        public int NoOfDwellers { get; set; }
        public string Location { get; set; }
        public string Weather { get; set; }

        public int CountryID { get; set; }
    }
}