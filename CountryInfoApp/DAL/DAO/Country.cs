﻿namespace CountryInfoApp.DAL.DAO
{
    public class Country
    {
        public string Name { get; set; }
        public string About { get; set; }
        public int ID { get; set; }
    }
}