﻿using System.Collections.Generic;
using System.Data.SqlClient;
using CountryInfoApp.DAL.DAO;


namespace CountryInfoApp.DAL.Gateway
{
    public class CountryGateway
    {
        Gateway aGateway = new Gateway();

        public int Save(Country aCountry)
        {
            string query = "insert into CountryTable(CountryName,About) values('" + aCountry.Name + "','" +
                           aCountry.About + "')";
            aGateway.sqlConnection.Open();
            aGateway.command.CommandText = query;
            int rowAffected = aGateway.command.ExecuteNonQuery();
            aGateway.sqlConnection.Close();
            return rowAffected;
        }

        public List<Country> GetCountryList()
        {
            List<Country> countryList=new List<Country>();
            aGateway.command.CommandText = "SELECT * FROM CountryTable ORDER BY CountryName ASC";
            aGateway.sqlConnection.Open();
            SqlDataReader reader = aGateway.command.ExecuteReader();
            int counter = 1;
            while (reader.Read())
            {
                Country newCountry=new Country();
                newCountry.Name = reader[1].ToString();
                newCountry.About = reader[2].ToString();
                newCountry.ID = counter;

               countryList.Add(newCountry);
               counter++;
            }
            reader.Close();
            aGateway.sqlConnection.Close();
            return countryList;



        }

        public int GetID(string Name)
        {
            int result=0;
            aGateway.command.CommandText = "SELECT * FROM CountryTable WHERE CountryName='" + Name + "'";
            aGateway.sqlConnection.Open();
            SqlDataReader reader = aGateway.command.ExecuteReader();
            while (reader.Read())
            {
                result = int.Parse(reader[0].ToString());
                
            }
            reader.Close();
            aGateway.sqlConnection.Close();
            return result;
            
        }

        public bool CheckCountryName(string CountryName)
        {
            bool result = false;
            aGateway.command.CommandText = "SELECT * FROM CountryTable WHERE CountryName='"+CountryName+"'";
            aGateway.sqlConnection.Open();
            SqlDataReader reader = aGateway.command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            reader.Close();
            aGateway.sqlConnection.Close();
            return result;
        }
    }
}