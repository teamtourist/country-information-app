﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CountryInfoApp.DAL.DAO;

namespace CountryInfoApp.DAL.Gateway
{
    public class CityGateway
    {
        Gateway aGateway=new Gateway();
        public int SaveCity(City aCity)
        {
            string query = "insert into CityEntryTable(Name,About,NoOfDwellers,Location,Weather,CountryID) values('" + aCity.Name + "','" + aCity.About + "','" + aCity.NoOfDwellers + "','" + aCity.Location + "','" + aCity.Weather + "','"+aCity.CountryID+"')";
            aGateway.sqlConnection.Open();
            aGateway.command.CommandText = query;
            int rowAffected = aGateway.command.ExecuteNonQuery();
            aGateway.sqlConnection.Close();
            return rowAffected;
        }
        public List<City> GetAllCityList()
        {
            List<City> cityList = new List<City>();
            aGateway.command.CommandText = "select * from CityEntryTable order by Name ASC";
            aGateway.sqlConnection.Open();
            SqlDataReader reader = aGateway.command.ExecuteReader();
            int counter = 1;
            while (reader.Read())
            {
                City newCity = new City();

                newCity.Name = reader[1].ToString();
                newCity.About = reader[2].ToString();
                newCity.NoOfDwellers = int.Parse(reader[3].ToString());
                newCity.Location = reader[4].ToString();
                newCity.Weather = reader[5].ToString();
                newCity.CountryID = int.Parse(reader[6].ToString());

                cityList.Add(newCity);
                counter++;

            }
            reader.Close();
            aGateway.sqlConnection.Close();
            return cityList;


        }

        public bool CheckCityName(string cityName)
        {
            bool result = false;
            aGateway.command.CommandText = "SELECT * FROM CityEntryTable WHERE Name='" + cityName + "'";
            aGateway.sqlConnection.Open();
            SqlDataReader reader = aGateway.command.ExecuteReader();
            while (reader.Read())
            {
                result = true;
                break;
            }
            reader.Close();
            aGateway.sqlConnection.Close();
            return result;


        }
    }
}