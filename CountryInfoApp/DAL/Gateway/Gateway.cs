﻿using System.Data.SqlClient;
using System.Web.Configuration;

namespace CountryInfoApp.DAL.Gateway
{
    public class Gateway
    {
        private string connect =WebConfigurationManager.ConnectionStrings["ConnectionLink"].ConnectionString;
       private SqlConnection connectionPath;
       private SqlCommand sqlcommmand;
        
        public Gateway()
        {
            connectionPath = new SqlConnection(connect);
            sqlcommmand=new SqlCommand();
            
        }
        public SqlConnection sqlConnection
        {
            get
            {
                return connectionPath;

            }
        }
        public SqlCommand command
        {
            get
            {
                sqlcommmand.Connection = connectionPath;
                return sqlcommmand;
            }
        }
    }
}