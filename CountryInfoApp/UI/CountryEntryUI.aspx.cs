﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CountryInfoApp.BLL;
using CountryInfoApp.DAL;
using CountryInfoApp.DAL.DAO;

namespace CountryInfoApp.UI
{
    public partial class CountryEntryUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           ShowCountryInfo();
        
        }
         Country aCountry=new Country();
       static CountryManager aCountryManager=new CountryManager();
        
        protected void saveButton_Click(object sender, EventArgs e)
        {
            aCountry.Name = nameTextBox.Value;
            if (aCountryManager.CountryNameExist(aCountry.Name)==true)
            {
                string script = "alert(\"Country Name Is Exist!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                    "ServerControlScript", script, true);
            }
            else
            {
                aCountry.About = Request.Form["aboutTextArea"];
                if (aCountryManager.Save(aCountry) == 1)
                {
                    string script = "alert(\"Saved Successfully!\");";
                    ScriptManager.RegisterStartupScript(this, GetType(),
                        "ServerControlScript", script, true);
                }
            }
            ShowCountryInfo();
            ClearTextBoxItem();
        }

        public void ClearTextBoxItem()
        {
            nameTextBox.Value = aboutTextArea.InnerText = "";
        }




            List<Country> countryList = aCountryManager.GetCountryList();
        public void ShowCountryInfo()
        {
            example.Rows.Clear();
            
            TableRow pRow = new TableRow();
            TableCell pcel = new TableCell();
            TableCell pcel2 = new TableCell();
            TableCell pcel3 = new TableCell();
            pcel.Text = "Serial No";
            pcel2.Text = "Country Name";
            pcel3.Text ="About";
            pRow.Cells.Add(pcel);
            pRow.Cells.Add(pcel2);
            pRow.Cells.Add(pcel3);
            example.Rows.Add(pRow);

            int count = 0;
           
            foreach (var country in countryList)
            { TableRow tRow = new TableRow();
               TableCell cel = new TableCell();
                TableCell cel2 = new TableCell();
                TableCell cel3 = new TableCell();
                cel.Text = (++count).ToString();
                cel2.Text = country.Name;
                cel3.Text = country.About;
                tRow.Cells.Add(cel);
                tRow.Cells.Add(cel2);
                tRow.Cells.Add(cel3);
                example.Rows.Add(tRow);
            }
            

          
        }

       
 

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            ClearTextBoxItem();
        }
        //protected void countryInfoGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    //for (int i = 1; i < 4; i++)
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            string decode = HttpUtility.HtmlDecode(e.Row.Cells[4].Text);
        //            e.Row.Cells[4].Text = decode;
        //        }
        //    }
        //}
    }
}



