﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MenuUI.aspx.cs" Inherits="CountryInfoApp.UI.MenuUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Contry Management</title> 
    <link href="../Content/MenuUI.css" rel="stylesheet" />
    <link href="../Content/reset.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css' />
</head>
<body>
   
    <form id="form1" runat="server">
    <div class="Main" >
        <div class="Header">
            <img src="../Images/FirstBanner.JPG" />
            <div class="Option">
                <ul>
                    <li><a href="MenuUI.aspx">Home</a></li>
                    <li><a href="CountryEntryUI.aspx">Country Entry</a></li>
                    <li><a href="CityEntryUI.aspx">City Entry</a></li>
                    <li><a href="ViewCitiesUI.aspx">View Cities Information</a></li>
                    <li><a href="ViewCountryUI.aspx">View Country's Information</a></li>
                </ul>


            </div>
            <div class="Description">
                <h2>Country Infomation Application & It's Feature</h2>
                <p>It is a country information application. Here, you can get all the information related to a countryThe word country comes from Old French cuntrée, itself derived from Vulgar Latin (terra) contrata (“(land) lying opposite; (land) spread before”), derived from contra (“against, opposite”). It most likely entered English language after the Franco-Norman invasion during the 11th century.

In English the word has increasingly become associated with political divisions, so that one sense, associated with the indefinite article – "a country" – is now a synonym for state, or a former sovereign state, in the sense of sovereign territory or "district, native land".[5] Areas much smaller than a political state may be called by names such as the West Country in England, the Black Country (a heavily industrialized part of England), "Constable Country" (a part of East Anglia painted by John Constable), the "big country" (used in various contexts of the American West), "coal country" (used of parts of the US and elsewhere) and many other terms.[6]

The equivalent terms in French and other Romance languages (pays and variants) have not carried the process of being identified with political sovereign states as far as the English "country", instead derived from, pagus, which designated the territory controlled by a medieval count, a title originally granted by the Roman Church. In many European countries the words are used for sub-divisions of the national territory, as in the German Bundesländer, as well as a less formal term for a sovereign state. France has very many "pays" that are officially recognised at some level, and are either natural regions, like the Pays de Bray, or reflect old political or economic unities, like the Pays de la Loire. At the same time the United States and Brazil are also "pays" in everyday French speech.

A version of "country" can be found in the modern French language as contrée, based on the word cuntrée in Old French,[6] that is used similarly to the word "pays" to define regions and unities, but can also be used to describe a political state in some particular cases. The modern Italian contrada is a word with its meaning varying locally, but usually meaning a ward or similar small division of a town, or a village or hamlet in the countryside.</p>
                
                
                
                

            </div>
            <div class="Footer">
                &copy; Team Tourist 

            </div>
    </div>
        </div>
    </form>
        
</body>
</html>
