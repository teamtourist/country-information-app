﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCitiesUI.aspx.cs" Inherits="CountryInfoApp.UI.ViewCitiesUI" ValidateRequest="False"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/MenuUI.css" rel="stylesheet" />
    <link href="../Content/reset.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css' /> 
</head>
<body>
    <form id="form1" runat="server">
        
         <div class="Main" >
        <div class="Header">
            <img src="../Images/FirstBanner.JPG" />
            <div class="Option">
                <ul>
                    <li><a href="MenuUI.aspx">Home</a></li>
                    <li><a href="CountryEntryUI.aspx">Country Entry</a></li>
                    <li><a href="CityEntryUI.aspx">City Entry</a></li>
                    <li><a href="ViewCitiesUI.aspx">View Cities Information</a></li>
                    <li><a href="ViewCountryUI.aspx">View Country's Information</a></li>
                </ul>

            </div>
             </div>
             
        

    <div>
    
        View Cities<br />
        <br />
        <asp:RadioButton ID="cityRadioButton" runat="server" Text="City Name" OnCheckedChanged="cityRadioButton_CheckedChanged" />
        <asp:TextBox ID="cityNameTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:RadioButton ID="countryRadioButton" runat="server" Text="Country Name" />
        <asp:DropDownList ID="searchByCountryDropDownList" runat="server">
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="searchButton" runat="server" Text="Search" OnClick="searchButton_Click" />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField />
                <asp:BoundField />
                <asp:BoundField />
                <asp:BoundField />
            </Columns>
        </asp:GridView>
         <div class="Footer">
                &copy; Team Tourist 

            </div>
     </div>
    </div>
    </form>
</body>
</html>
