﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CityEntryUI.aspx.cs" Inherits="CountryInfoApp.UI.CityEntryUI" ValidateRequest="False" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Country Management</title>

    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css' />
    <link href="../Content/font-awesome.css" rel="stylesheet" />
    <link href="../Editor/css/froala_editor.css" rel="stylesheet" />
    <link href="../Editor/css/froala_style.css" rel="stylesheet" />
    <link href="../Content/MenuUI.css" rel="stylesheet" />
    <link href="../Content/reset.css" rel="stylesheet" />
    <link href="../Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" />
</head>
<body>

    <form id="form1" runat="server">
        <div class="Main">
            <div class="Header">
                <img src="../Images/FirstBanner.JPG" />
                <div class="Option">
                    <ul>
                        <li><a href="MenuUI.aspx">Home</a></li>
                        <li><a href="CountryEntryUI.aspx">Country Entry</a></li>
                        <li><a href="CityEntryUI.aspx">City Entry</a></li>
                        <li><a href="ViewCitiesUI.aspx">View Cities Information</a></li>
                        <li><a href="ViewCountryUI.aspx">View Country's Information</a></li>
                    </ul>

                </div>
            </div>
            <div class="CityEntry">
                City Entry<br />
                Name
        <asp:TextBox ID="nameTextBox" runat="server"></asp:TextBox>
                <br />
                <br />
                About
         <textarea id="aboutTextArea" runat="server" cols="20" name="S1"></textarea><br />
                <br />
                <br />
                No Of Dwellers&nbsp;
                    <asp:TextBox ID="noOfDwellers" runat="server"></asp:TextBox>
                <br />
                <br />
                Location
        <asp:TextBox ID="locationTextBox" runat="server"></asp:TextBox>
                <br />
                <br />
                Weather
        <asp:TextBox ID="weatherTextBox" runat="server"></asp:TextBox>
                <br />
                <br />
                Country
        <asp:DropDownList ID="countryDropDownList" runat="server">
        </asp:DropDownList>
                <br />
                <br />
                <br />
                <asp:Button ID="saveButton" runat="server" Text="Save" OnClick="saveButton_Click" />
                &nbsp;&nbsp;&nbsp;
        <asp:Button ID="cancelButton" runat="server" Text="Cancel" />
                <br />

                <br />


            </div>
            <div class="DataTableDiv">

                <asp:Table ID="example" class="display" CellSpacing="0" Width="100%" runat="server">
                    <asp:TableRow>

                        <asp:TableCell>Serial No</asp:TableCell>
                        <asp:TableCell>City Name</asp:TableCell>
                        <asp:TableCell>No of Dwellers</asp:TableCell>
                        <asp:TableCell>Country</asp:TableCell>


                    </asp:TableRow>


                </asp:Table>

                <div class="Footer">
                    &copy; Team Tourist 

                </div>
            </div>


        </div>
    </form>
     <script src="../Scripts/jquery-2.1.4.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <script src="../Editor/js/froala_editor.min.js"></script>
    <script src="../Editor/js/plugins/block_styles.min.js"></script>
    <script src="../Editor/js/plugins/char_counter.min.js"></script>
    <script src="../Editor/js/plugins/colors.min.js"></script>
    <script src="../Editor/js/plugins/entities.min.js"></script>
    <script src="../Editor/js/plugins/file_upload.min.js"></script>
    <script src="../Editor/js/plugins/font_family.min.js"></script>
    <script src="../Editor/js/plugins/font_size.min.js"></script>
    <script src="../Scripts/DataTables/jquery.dataTables.min.js"></script>
     <script>

         $(document).ready(function () {
            
             $("#example").dataTable({

             });


         });

    </script>
</body>

</html>

x