﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryEntryUI.aspx.cs" Inherits="CountryInfoApp.UI.CountryEntryUI" ValidateRequest="False" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Country Entry</title>
    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css' />
    <link href="../Content/font-awesome.css" rel="stylesheet" />
    <link href="../Editor/css/froala_editor.css" rel="stylesheet" />
    <link href="../Editor/css/froala_style.css" rel="stylesheet" />
    <link href="../Content/reset.css" rel="stylesheet" />
    <link href="../Content/CountryEntryUI.css" rel="stylesheet" />
    <link href="../Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <link href="../Content/DataTables/css/jquery.dataTables.css" rel="stylesheet" />

    <style type="text/css">
        #aboutTextArea {
            height: 25px;
            margin-top: 0px;
        }

        label.error {
            color: red;
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="countryEntryForm" runat="server">
        <div class="Main">
            <div class="Header">
                <img src="../Images/FirstBanner.JPG" />
                <div class="Option">
                    <ul>
                        <li><a href="MenuUI.aspx">Home</a></li>
                        <li><a href="CountryEntryUI.aspx">Country Entry</a></li>
                        <li><a href="CityEntryUI.aspx">City Entry</a></li>
                        <li><a href="ViewCitiesUI.aspx">View Cities Information</a></li>
                        <li><a href="ViewCountryUI.aspx">View Country's Information</a></li>
                    </ul>


                </div>

            </div>
                <div class="Description" id="Description">

                    <div class="InfoEntry">
                        <h3>Country Entry Form</h3>
                    </div>
                    <div class="CountryName" id="CountryEntryLayout">
                        <h1>Country Name</h1>

                        <input type="text" name="nameTextBox" id="nameTextBox" runat="server" />
                        <br />
                        <br />
                        About
            <textarea id="aboutTextArea" runat="server" cols="20" name="S1"></textarea><br />
                        <br />
                        &nbsp;<asp:Button ID="saveButton" runat="server" Text="Save" OnClick="saveButton_Click" />
                        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="cancelButton" runat="server" Text="Cancel" OnClick="cancelButton_Click" />
                        <br />
                       <div class="DataTableDiv" runat="server">
                            
                                                   
                    <asp:table id="example" class="display" cellspacing="0" width="100%" runat="server" Height="200px">
                        <asp:TableRow>
                           
                                <asp:TableCell>Serial No</asp:TableCell>
                                <asp:TableCell>Country Name</asp:TableCell>
                                <asp:TableCell >About</asp:TableCell>

                            
                        </asp:TableRow>

                       
                    </asp:table>
                </div>
                        <br />

                    </div>


                
                



            </div>
            <div class="Footer">
                &copy; Team Tourist 

            </div>
        </div>

    </form>
    <script src="../Scripts/jquery-2.1.4.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <script src="../Editor/js/froala_editor.min.js"></script>
    <script src="../Editor/js/plugins/block_styles.min.js"></script>
    <script src="../Editor/js/plugins/char_counter.min.js"></script>
    <script src="../Editor/js/plugins/colors.min.js"></script>
    <script src="../Editor/js/plugins/entities.min.js"></script>
    <script src="../Editor/js/plugins/file_upload.min.js"></script>
    <script src="../Editor/js/plugins/font_family.min.js"></script>
    <script src="../Editor/js/plugins/font_size.min.js"></script>
    <script src="../Scripts/DataTables/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $("#aboutTextArea").editable({
                inlineMode: false,
                height: 200,
                width: 750,
                alwaysBlank: true
            });
            $("#countryEntryForm").validate({
                rules: {
                    nameTextBox: "required"

                },
                messages: {
                    nameTextBox: "Please Enter Country Name"
                }

            });
            $("#example").dataTable({

            });


        });

    </script>

</body>
</html>






