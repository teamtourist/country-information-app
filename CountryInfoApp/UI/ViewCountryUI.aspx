﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewCountryUI.aspx.cs" Inherits="CountryInfoApp.UI.ViewCountryUI" ValidateRequest="False" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/MenuUI.css" rel="stylesheet" />
    <link href="../Content/reset.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css'/> 
</head>
<body>
    <form id="form1" runat="server">
        <div class="Main" >
        <div class="Header">
            <img src="../Images/FirstBanner.JPG" />
            <div class="Option">
                <ul>
                    <li><a href="MenuUI.aspx">Home</a></li>
                    <li><a href="CountryEntryUI.aspx">Country Entry</a></li>
                    <li><a href="CityEntryUI.aspx">City Entry</a></li>
                    <li><a href="ViewCitiesUI.aspx">View Cities Information</a></li>
                    <li><a href="ViewCountryUI.aspx">View Country's Information</a></li>
                </ul>

            </div>
            </div>
    <div>
        View Country
        <br />
        Search Criteria<br />
        <br />
        Name
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="searchButton" runat="server" Height="26px" Text="Search" />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField />
                <asp:BoundField />
                <asp:BoundField />
                <asp:BoundField />
                <asp:BoundField />
            </Columns>
        </asp:GridView>
        <br />
        <br />
         <div class="Footer">
                &copy; Team Tourist 

            </div>

    
    </div>
            </div>
    </form>
</body>
</html>
