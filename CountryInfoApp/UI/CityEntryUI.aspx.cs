﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CountryInfoApp.BLL;
using CountryInfoApp.DAL.DAO;

namespace CountryInfoApp.UI
{
    public partial class CityEntryUI : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetCountry();
            ShowCityInfo();
            
            if (!IsPostBack)
            {
                LoadDropDownList(); ShowCityInfo();
            }
            

        }
        City aCity=new City();
        Country aCountry=new Country();
        CityManager aCityManager=new CityManager();
       static CountryManager aCountryManager=new CountryManager();
        Dictionary<int,string>getCountryList=new Dictionary<int, string>();
        List<Country> aCountryList = aCountryManager.GetCountryList();
        protected void saveButton_Click(object sender, EventArgs e)
        {
            var item =  countryDropDownList.SelectedItem.Value;

            aCity.Name = nameTextBox.Text;
            aCity.About = Request.Form["aboutTextArea"];
            aCity.NoOfDwellers = int.Parse(noOfDwellers.Text);
            aCity.Location = locationTextBox.Text;
            aCity.Weather = weatherTextBox.Text;
            aCity.CountryID =int.Parse(item);
            if (aCityManager.CheckCity(aCity.Name)==true)
            {
                 string script = "alert(\"Country Name Is Exist!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                    "ServerControlScript", script, true);

            }
            else{

            if(aCityManager.SaveCity(aCity)==1)
            {
                
                string script = "alert(\"Saved Successfully!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);
            }
            }
            ClearTextBoxItem();
        }

        public void ClearTextBoxItem()
        {
            nameTextBox.Text = aboutTextArea.InnerText =noOfDwellers.Text=locationTextBox.Text=weatherTextBox.Text= "";
        }

        public void ShowCityInfo()
        {
            example.Rows.Clear();
            List<City> cityList = aCityManager.GetAllCity();
            TableRow pRow = new TableRow();
            TableCell pcel = new TableCell();
            TableCell pcel2 = new TableCell();
            TableCell pcel3 = new TableCell();
            TableCell pcel4 = new TableCell();
            pcel.Text = "Serial No";
            pcel2.Text = "City Name";
            pcel3.Text = "Number of Dwellers";
            pcel4.Text = "Country";

            pRow.Cells.Add(pcel);
            pRow.Cells.Add(pcel2);
            pRow.Cells.Add(pcel3);
            pRow.Cells.Add(pcel4);
            example.Rows.Add(pRow);

            int count = 0;

            foreach (var city in cityList)
            {
                TableRow tRow = new TableRow();
                TableCell cel = new TableCell();
                TableCell cel2 = new TableCell();
                TableCell cel3 = new TableCell();
                TableCell cel4 = new TableCell();
                cel.Text = (++count).ToString();
                cel2.Text = city.Name;
                cel3.Text = city.NoOfDwellers.ToString();
                cel4.Text = getCountryList[city.CountryID];
                tRow.Cells.Add(cel);
                tRow.Cells.Add(cel2);
                tRow.Cells.Add(cel3);
                tRow.Cells.Add(cel4);
                example.Rows.Add(tRow);
            }
            

            
        }

        public void LoadDropDownList()
        {
            List<string> countryName=new List<string>();
            
           
            countryDropDownList.DataSource = aCountryList;
            countryDropDownList.DataTextField = "Name";
            countryDropDownList.DataValueField = "ID";
            countryDropDownList.DataBind();



        }

        public void GetCountry()
        {
            foreach (var country in aCountryList)
            {
                getCountryList.Add(country.ID,country.Name);
            }
        }


    }
  
}