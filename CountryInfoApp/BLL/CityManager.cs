﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CountryInfoApp.DAL.DAO;
using CountryInfoApp.DAL.Gateway;

namespace CountryInfoApp.BLL
{
    public class CityManager
    {
        CityGateway aCityGatewaye=new CityGateway();
        public int SaveCity(City aCity)
        {
            return aCityGatewaye.SaveCity(aCity);
        }

        public List<City> GetAllCity()
        {
            return aCityGatewaye.GetAllCityList();
        }

        public bool CheckCity(string cityName)
        {
            return aCityGatewaye.CheckCityName(cityName);
        }
    }
}