﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CountryInfoApp.DAL.DAO;
using CountryInfoApp.DAL.Gateway;

namespace CountryInfoApp.BLL
{
    public class CountryManager
    {
        private CountryGateway aCountryGateway = new CountryGateway();

        public int Save(Country aCountry)
        {
            return aCountryGateway.Save(aCountry);
        }

        public List<Country> GetCountryList()
        {
            return aCountryGateway.GetCountryList();

        }

        public bool CountryNameExist(string countryName)
        {
            return aCountryGateway.CheckCountryName(countryName);
        }

        public int GetCountryID(string countryName)
        {
            return aCountryGateway.GetID(countryName);
        }
    }
}
